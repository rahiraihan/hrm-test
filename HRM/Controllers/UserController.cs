﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRM.Data.ViewModels;
using HRM.Service.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : CustomController
    {
 
           private IUserService _service;

            public UserController(IUserService service)
            {
                _service = service;
            }
        [HttpGet]
        
        public async Task<IActionResult> GetUser(int id)
        {
            try
            {
                //Convert.ToInt32
                int userId = await GetUserIdFromClaim();
                var res = await _service.GetUser(userId);

                if (res != null)
                {

                    return Ok(res);
                }
                return StatusCode(StatusCodes.Status204NoContent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [AllowAnonymous]
        [HttpPost("Login")]
        
        public async Task<IActionResult> Login([FromBody]vmLogin model)
            {
                try
                {
                    //Convert.ToInt32
                   // int userId = await GetUserIdFromClaim();
                    var res = await _service.LoginUserTokenBase(model.Username, model.Password); ;

                    if (res != null)
                    {

                        return Ok(res);
                    }
                    return StatusCode(StatusCodes.Status204NoContent);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
}