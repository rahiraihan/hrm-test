﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomController : ControllerBase
    {
       
        public CustomController()
        {

        }
        protected async Task<int> GetUserIdFromClaim()
        {

            var principal = this.HttpContext.User;
            if (principal == null) throw new Exception("User not found");
            var claims = principal.Claims.ToList();
            var userId = claims.FirstOrDefault(c => c.Value == "UserId")?.Value;
            return await System.Threading.Tasks.Task.FromResult(Convert.ToInt32(userId));


        }

        //protected async Task<int> GetEmailIdFromClaim() {
        //    var identity = (ClaimsIdentity)User.Identity;
        //    var email = identity.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).SingleOrDefault();
        //    return await email;
        //}


        //protected async Task<int> GetUserIdFromClaim()
        //{
        //    return int.Parse(this.User.Claims.First(i => i.Type == "UserId").Value);
        //}


        //public async Task<int> GetUserIdFromClaim(this ClaimsPrincipal claim)
        //{
        //    var principal = this.HttpContext.User;
        //    var userID = principal.Claims.ToList().Find(r => r.Type == "UserID").Value;
        //    return Convert.ToInt32(userID);
        //}
        //public async Task<int> CurrentUserRole(this ClaimsPrincipal claim)
        //{
        //    var principal = this.HttpContext.User;
        //    var role = principal.Claims.ToList().Find(r => r.Type == "Role").Value;
        //    return  Convert.ToInt32(role);
        //}
    }
}