﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRM.Service.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaxController : CustomController
    {
        private readonly ITaxService _service;
     
        public TaxController(ITaxService service)
        {
            _service = service;
      
        }
        [HttpGet]
        [EnableCors("CorsPolicy")]
        public async Task<IActionResult> GetTax(int id)
        {
            try
            {
                //Convert.ToInt32
                int userId = await GetUserIdFromClaim();
                var res = await _service.GetTax(userId);

                if (res != null)
                {

                    return Ok(res);
                }
                return StatusCode(StatusCodes.Status204NoContent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}