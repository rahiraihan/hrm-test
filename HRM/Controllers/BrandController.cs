﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRM.Service.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrandController : CustomController
    {
        private readonly IBrandService _service;

        public BrandController(IBrandService service)
        {
            _service = service;
        }

        [HttpGet]
        [EnableCors("CorsPolicy")]
        public async Task<IActionResult> GetBrands(int id)
        {
            try
            {
                //Convert.ToInt32
                int userId =   await GetUserIdFromClaim();
                var res = await _service.GetAllBrands(userId);

                if (res != null)
                {

                    return Ok(res);
                }
                return StatusCode(StatusCodes.Status204NoContent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}