export interface AddTax{

    AddTaxesId: number;
    TaxName: string; 
    TaxValue: number; 
    Status: string;
}