import { Component, OnInit , Inject} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddTaxService } from './services/add-tax.service';

@Component({
  selector: 'app-add-tax',
  templateUrl: './add-tax.component.html',
  styleUrls: ['./add-tax.component.css']
})
export class AddTaxComponent implements OnInit {
  public _taxForm: FormGroup;

  constructor(private _formBuilder: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private _taxservice: AddTaxService,
              private dialogRef: MatDialogRef<AddTaxComponent>) { }

  ngOnInit() {
    this._taxForm = this._formBuilder.group({
      AddTaxesId: [this.data.AddTaxesId],
      TaxName: [ this.data.TaxName, [Validators.required]],
      TaxValue: [ this.data.TaxValue, [Validators.required]],
      Status: [ this.data.Status, [Validators.required]],
    
    
  });
  }
  onSubmit() {
    if (isNaN(this.data.ID)) {
      this._taxservice.addAddTax(this._taxForm.value);
      this.dialogRef.close();
    } else {
      this._taxservice.deleteAddTax(this._taxForm.value);
      this.dialogRef.close();
    }
  }
  onClose() {
    this.dialogRef.close();
  }

}