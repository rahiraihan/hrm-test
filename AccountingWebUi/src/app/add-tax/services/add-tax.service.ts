import { Injectable } from '@angular/core';
import { AddTax } from '../models/model';

@Injectable({
  providedIn: 'root'
})
export class AddTaxService {

  constructor() { }
  _addTaxList: AddTax[] = [
    // {AddTaxesId: 1, TaxName: 'Hydrogen', TaxValue: 1.0079, Status: 'H'},
    // {AddTaxesId: 1, TaxName: 'Helium', TaxValue: 1.0079, Status: 'He'},
    // {AddTaxesId: 1, TaxName: 'Lithium', TaxValue: 1.0079, Status: 'Li'},
    // {AddTaxesId: 1, TaxName: 'Beryllium', TaxValue: 1.0079, Status: 'Be'},
    // {AddTaxesId: 1, TaxName: 'Boron', TaxValue: 1.0079, Status: 'B'},
    // {AddTaxesId: 1, TaxName: 'Hydrogen', TaxValue: 1.0079, Status: 'H'},
    // {AddTaxesId: 1, TaxName: 'Helium', TaxValue: 1.0079, Status: 'He'},
    // {AddTaxesId: 1, TaxName: 'Lithium', TaxValue: 1.0079, Status: 'Li'},
    // {AddTaxesId: 1, TaxName: 'Beryllium', TaxValue: 1.0079, Status: 'Be'},
    // {AddTaxesId: 1, TaxName: 'Boron', TaxValue: 1.0079, Status: 'B'},
    
  ];

  addAddTax(addTax: AddTax) {
    addTax.AddTaxesId = this._addTaxList.length + 1;
    this._addTaxList.push(addTax);
  }

  editAddTax(addTax: AddTax) {
    const index = this._addTaxList.findIndex(c => c.AddTaxesId === addTax.AddTaxesId);
    this._addTaxList[index] = addTax;
  }

  deleteAddTax(id: number) {
    const addTax = this._addTaxList.findIndex(c => c.AddTaxesId === id);
    this._addTaxList.splice(addTax, 1);
  }

  getAddTax() {
    return this._addTaxList;
  }
}
