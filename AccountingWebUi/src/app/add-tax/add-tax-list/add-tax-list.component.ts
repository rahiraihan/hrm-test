import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog, MatDialogConfig  } from '@angular/material';
import { AddTaxService } from '../services/add-tax.service';
import { AddTaxComponent } from '../add-tax.component';

@Component({
  selector: 'app-add-tax-list',
  templateUrl: './add-tax-list.component.html',
  styleUrls: ['./add-tax-list.component.css']
})
export class AddTaxListComponent implements OnInit {
  //listData: MatTableDataSource<any>;
  isPopupOpened: true;
  displayedColumns: string[] = ['addtaxid', 'taxname', 'taxvalue', 'status', 'actions'];
  dataSource = new MatTableDataSource(this.service._addTaxList);

  constructor(private dialog: MatDialog , private service: AddTaxService) { }

  ngOnInit() {
    
  }
  get addTaxtList() {
    return this.service.getAddTax();
  }

  addAddTax() {
    const dialogConfig = new MatDialogConfig();
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(AddTaxComponent, {
      data: {}
    });


    dialogRef.afterClosed().subscribe(result => {
    });
    dialogConfig.width = '60px';
   
  }

  editAddTax(id: number) {
    this.isPopupOpened = true;
    const tax = this.service.getAddTax().find(c => c.AddTaxesId === id);
    const dialogRef = this.dialog.open(AddTaxComponent, {
      data: {}
    });


    dialogRef.afterClosed().subscribe(result => {
     data: tax
    });
  }

  deleteAddTax(id: number) {
    this.service.deleteAddTax(id);
  }

}
