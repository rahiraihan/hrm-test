export interface Employees
{
  EmployeesId: number;
  EmployeesName: string;
  Designation: string;
  MobileNo: string;
}
