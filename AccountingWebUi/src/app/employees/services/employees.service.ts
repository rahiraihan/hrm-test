import { Injectable } from '@angular/core';
import { Employees } from '../models/employees';




@Injectable({
  providedIn: 'root'
})
export class EmployeesService{
  constructor() { }

  _employees: Employees [] = [
    {EmployeesId: 1, EmployeesName: 'Rahi', Designation: 'Executive', MobileNo: '+8801823578649'},
    {EmployeesId: 1, EmployeesName: 'Rahi', Designation: 'Executive', MobileNo: '+8801823578649'},
    {EmployeesId: 1, EmployeesName: 'Rahi', Designation: 'Executive', MobileNo: '+8801823578649'},
    {EmployeesId: 1, EmployeesName: 'Rahi', Designation: 'Executive', MobileNo: '+8801823578649'},
  ];

  addEmployee(addEmployees: Employees) {
    addEmployees.EmployeesId = this._employees.length + 1;
    this._employees.push(addEmployees);
  }

  editEmployeeDetail(addEmployees: Employees) {
    const index = this._employees.findIndex(c => c.EmployeesId === addEmployees.EmployeesId);
    this._employees[index] = addEmployees;
    this._employees.push(addEmployees);
  }

  deleteEmployeeDetail(id: number) {
    const employee = this._employees.findIndex(c => c.EmployeesId === id);
    this._employees.splice(employee, 1);
  }

  getEmployee() {
    return this._employees;
  }


}
