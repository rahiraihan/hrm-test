import { Injectable } from '@angular/core';
import { Customers } from '../models/customers';



@Injectable({
  providedIn: 'root'
})
export class CustomersService{
  constructor() { }

  _customers: Customers [] = [
    {CustomerId: 1, Name: 'Rahi', Company: 'OptoCoder', CompanyAddress: 'Mirpur'},
    {CustomerId: 1, Name: 'Rahi', Company: 'OptoCoder', CompanyAddress: 'Mirpur'},
    {CustomerId: 1, Name: 'Rahi', Company: 'OptoCoder', CompanyAddress: 'Mirpur'},
    {CustomerId: 1, Name: 'Rahi', Company: 'OptoCoder', CompanyAddress: 'Mirpur'},
    {CustomerId: 1, Name: 'Rahi', Company: 'OptoCoder', CompanyAddress: 'Mirpur'},
    {CustomerId: 1, Name: 'Rahi', Company: 'OptoCoder', CompanyAddress: 'Mirpur'},
  ];

  addCustomer(addCustomers: Customers) {
    addCustomers.CustomerId = this._customers.length + 1;
    this._customers.push(addCustomers);
  }

  editCustomerDetail(addCustomers: Customers) {
    const index = this._customers.findIndex(c => c.CustomerId === addCustomers.CustomerId);
    this._customers[index] = addCustomers;
    this._customers.push(addCustomers);
  }

  deleteCustomerDetail(id: number) {
    const customer = this._customers.findIndex(c => c.CustomerId === id);
    this._customers.splice(customer, 1);
  }

  getCustomer() {
    return this._customers;
  }
 

}