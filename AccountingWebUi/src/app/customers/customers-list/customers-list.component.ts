import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../services/customers.service';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.css']
})
export class CustomersListComponent implements OnInit {

  constructor( private service: CustomersService) { }

  ngOnInit() {
  }

  get customerList() {
    return this.service.getCustomer();
  }

}
