import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatSortModule} from '@angular/material/sort';
import { ReactiveFormsModule,FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddTaxListComponent } from './add-tax/add-tax-list/add-tax-list.component';
import { AccountGroupDetailComponent } from './account-group-detail/account-group-detail.component';
import { AccountGroupDetailListComponent } from './account-group-detail/account-group-detail-list/account-group-detail-list.component';
import { AddTaxComponent } from './add-tax/add-tax.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BrandsAddComponent } from './brands-add/brands-add.component';
import { BrandsAddListComponent } from './brands-add/brands-add-list/brands-add-list.component';
import { RouterModule } from '@angular/router';
import { CustomersComponent } from './customers/customers.component';
import { CustomersListComponent } from './customers/customers-list/customers-list.component';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeesListComponent } from './employees/employees-list/employees-list.component';
import { ProductsComponent } from './products/products.component';
import { ProductListComponent } from './products/product-list/product-list.component';





@NgModule({
  declarations: [
    AppComponent,
    AddTaxComponent,
    AddTaxListComponent,
    AccountGroupDetailComponent,
    AccountGroupDetailListComponent,
    NavbarComponent,
    DashboardComponent,
    BrandsAddComponent,
    BrandsAddListComponent,
    CustomersComponent,
    CustomersListComponent,
    EmployeesComponent,
    EmployeesListComponent,
    ProductsComponent,
    ProductListComponent,
   // FormsModule,
    //ReactiveFormsModule
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    CommonModule,
    MatToolbarModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatSnackBarModule,
    MatTableModule,
    MatIconModule,
    MatSortModule,
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    RouterModule.forRoot([
      { path: 'app-add-tax-list', component: AddTaxListComponent },
    { path: 'app-account-group-detail-list', component: AccountGroupDetailListComponent },
    { path: 'app-navbar', component: NavbarComponent },
    { path: '', component: DashboardComponent },
    { path: 'app-customers-list', component: CustomersListComponent },
    { path: 'app-employees-list', component: EmployeesListComponent },
    { path: 'app-Product-list', component: ProductListComponent },
    { path: '**', redirectTo: '/' }
    ])


  ],
  entryComponents: [AddTaxComponent,AccountGroupDetailComponent],
  exports: [
    // MatToolbarModule,
    // MatGridListModule,
    // MatFormFieldModule,
    // MatInputModule,
    // MatRadioModule,
    // MatSelectModule,
    // MatCheckboxModule,
    // MatDatepickerModule,
    // MatNativeDateModule,
    // MatButtonModule,
    // MatSnackBarModule,
    // MatTableModule,
    // MatIconModule,
    // MatSortModule,
    // MatDialogModule,

  ],
  providers: [],

  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
