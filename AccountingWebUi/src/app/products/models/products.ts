export interface Products{

  ProductID: number;
  ProductName: string;
  Location: string;
  ProductWarehouse: string;

}
