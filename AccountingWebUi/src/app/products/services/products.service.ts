import { Injectable } from '@angular/core';
import { Products } from '../models/products';




@Injectable({
  providedIn: 'root'
})
export class ProductsService{
  constructor() { }

  _products: Products [] = [
    {ProductID: 1, ProductName: 'Rahi', Location: 'OptoCoder', ProductWarehouse: 'Mirpur'},
    {ProductID: 1, ProductName: 'Rahi', Location: 'OptoCoder', ProductWarehouse: 'Mirpur'},
    {ProductID: 1, ProductName: 'Rahi', Location: 'OptoCoder', ProductWarehouse: 'Mirpur'},
    {ProductID: 1, ProductName: 'Rahi', Location: 'OptoCoder', ProductWarehouse: 'Mirpur'},
    {ProductID: 1, ProductName: 'Rahi', Location: 'OptoCoder', ProductWarehouse: 'Mirpur'},

  ];

  addProduct(addProducts: Products) {
    addProducts.ProductID = this._products.length + 1;
    this._products.push(addProducts);
  }

  editProductDetail(addProducts: Products) {
    const index = this._products.findIndex(c => c.ProductID === addProducts.ProductID);
    this._products[index] = addProducts;
    this._products.push(addProducts);
  }

  deleteProductDetail(id: number) {
    const product = this._products.findIndex(c => c.ProductID === id);
    this._products.splice(product, 1);
  }

  getProduct() {
    return this._products;
  }


}
