export interface BrandsAdd{

    BrandsAddId:number;
    BrandsName:string; 
    Manufacturer:string;
    Description: string;
    Status: string;
}