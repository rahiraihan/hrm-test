import { Injectable } from '@angular/core';
import { BrandsAdd } from '../models/brands-add';

@Injectable({
  providedIn: 'root'
})
export class BrandAddService {

    constructor() { }
    _brandList: BrandsAdd[] = [
      {BrandsAddId: 1, BrandsName: 'Hydrogen', Manufacturer: '1.0079',Description:'Done', Status: 'H'},
      {BrandsAddId: 1, BrandsName: 'Hydrogen', Manufacturer: '1.0079',Description:'Done', Status: 'H'},
      {BrandsAddId: 1, BrandsName: 'Hydrogen', Manufacturer: '1.0079',Description:'Done', Status: 'H'},
      {BrandsAddId: 1, BrandsName: 'Hydrogen', Manufacturer: '1.0079',Description:'Done', Status: 'H'},
      {BrandsAddId: 1, BrandsName: 'Hydrogen', Manufacturer: '1.0079',Description:'Done', Status: 'H'},
      {BrandsAddId: 1, BrandsName: 'Hydrogen', Manufacturer: '1.0079',Description:'Done', Status: 'H'},
  ];
  
    brandAdd(brand: BrandsAdd) {
        brand.BrandsAddId = this._brandList.length + 1;
      this._brandList.push(brand);
    }
  
    brandEdit(brand: BrandsAdd) {
      const index = this._brandList.findIndex(c => c.BrandsAddId === brand.BrandsAddId);
      this._brandList[index] = brand;
    }
  
    deleteBrand(id: number) {
      const brand = this._brandList.findIndex(c => c.BrandsAddId === id);
      this._brandList.splice(brand, 1);
    }
  
    getBrand() {
      return this._brandList;
    }
  }