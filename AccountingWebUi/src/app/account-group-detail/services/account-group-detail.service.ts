import { Injectable } from '@angular/core';
import { AccountGroupDetail } from '../models/accountgroupdetail';


@Injectable({
  providedIn: 'root'
})
export class AccountGroupDetailService{
  constructor() { }
  _addAccountDetail: AccountGroupDetail[] = [
    {AccountGroupDetailId: 1, AccountGroupName: 'Hydrogen', GroupUnder: 'acc', AffectGrossProfit: 8,ExtraDate:'sd',Narration:'asd',Nature:'re',Extra1:'ex1',Extra2:'ex2'},
    {AccountGroupDetailId: 1, AccountGroupName: 'Helium', GroupUnder: 'acc', AffectGrossProfit: 8,ExtraDate:'sd',Narration:'asd',Nature:'re',Extra1:'ex1',Extra2:'ex2'},
    {AccountGroupDetailId: 1, AccountGroupName: 'Lithium', GroupUnder: 'acc', AffectGrossProfit: 8,ExtraDate:'sd',Narration:'asd',Nature:'re',Extra1:'ex1',Extra2:'ex2'},
    {AccountGroupDetailId: 1, AccountGroupName: 'Boron', GroupUnder: 'acc', AffectGrossProfit: 8,ExtraDate:'sd',Narration:'asd',Nature:'re',Extra1:'ex1',Extra2:'ex2'},
    {AccountGroupDetailId: 1, AccountGroupName: 'Hydrogen', GroupUnder: 'acc', AffectGrossProfit: 8,ExtraDate:'sd',Narration:'asd',Nature:'re',Extra1:'ex1',Extra2:'ex2'},
    {AccountGroupDetailId: 1, AccountGroupName: 'Helium', GroupUnder: 'acc', AffectGrossProfit: 8,ExtraDate:'sd',Narration:'asd',Nature:'re',Extra1:'ex1',Extra2:'ex2'},
    {AccountGroupDetailId: 1, AccountGroupName: 'Lithium', GroupUnder: 'acc', AffectGrossProfit: 8,ExtraDate:'sd',Narration:'asd',Nature:'re',Extra1:'ex1',Extra2:'ex2'},
    {AccountGroupDetailId: 1, AccountGroupName: 'Beryllium', GroupUnder: 'acc', AffectGrossProfit: 8,ExtraDate:'sd',Narration:'asd',Nature:'re',Extra1:'ex1',Extra2:'ex2'},
    {AccountGroupDetailId: 1, AccountGroupName: 'Boron', GroupUnder: 'acc', AffectGrossProfit: 8,ExtraDate:'sd',Narration:'asd',Nature:'re',Extra1:'ex1',Extra2:'ex2'},
    
  ];
  addAccountDetail(addAccountDetail: AccountGroupDetail) {
    addAccountDetail.AccountGroupDetailId = this._addAccountDetail.length + 1;
    this._addAccountDetail.push(addAccountDetail);
  }

  editAccountDetail(addAccountDetail: AccountGroupDetail) {
    const index = this._addAccountDetail.findIndex(c => c.AccountGroupDetailId === addAccountDetail.AccountGroupDetailId);
    this._addAccountDetail[index] = addAccountDetail;
    this._addAccountDetail.push(addAccountDetail);
  }

  deleteAccountDetail(id: number) {
    const addTax = this._addAccountDetail.findIndex(c => c.AccountGroupDetailId === id);
    this._addAccountDetail.splice(addTax, 1);
  }

  getAccountDetail() {
    return this._addAccountDetail;
  }
    
}