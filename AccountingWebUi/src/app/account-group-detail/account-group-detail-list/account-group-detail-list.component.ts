import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource, MatDialogConfig } from '@angular/material';
import { AccountGroupDetailService } from '../services/account-group-detail.service';
import { AccountGroupDetailComponent } from '../account-group-detail.component';
import { AccountGroupDetail } from '../models/accountgroupdetail';

@Component({
  selector: 'app-account-group-detail-list',
  templateUrl: './account-group-detail-list.component.html',
  styleUrls: ['./account-group-detail-list.component.css']
})
export class AccountGroupDetailListComponent implements OnInit {
  isPopupOpened: true;
  displayedColumns: string[] = ['accountGroupDetailId', 'accountGroupName', 'groupUnder', 'affectGrossProfit','extraDate', 'narration', 'nature', 'extra1','extra2', 'actions'];
  dataSource = new MatTableDataSource<AccountGroupDetail>(this.service.getAccountDetail());

  constructor(private dialog: MatDialog , private service: AccountGroupDetailService) { }

  ngOnInit() {
   

  }
  get addAccountDetailList() {
    return this.service.getAccountDetail();
  }

  addAccountDetail() {
    const dialogConfig = new MatDialogConfig();
    this.isPopupOpened = true;
    const dialogRef = this.dialog.open(AccountGroupDetailComponent, {
      data: {}
    });


    dialogRef.afterClosed().subscribe(result => {
    });
    dialogConfig.width = '60px';
   
  }

  editAccountDetail(id: number) {
    this.isPopupOpened = true;
    const account = this.service.getAccountDetail().filter(c => c.AccountGroupDetailId === id);
    const dialogRef = this.dialog.open(AccountGroupDetailComponent, {
      data: {}
    });


    dialogRef.afterClosed().subscribe(result => {
     data: account
    });
  }

  deleteAccountDetail(id: number) : void{
    this.service.deleteAccountDetail(id);
  }
 

}
