export interface AccountGroupDetail{

         AccountGroupDetailId: number;
         AccountGroupName: string; 
         GroupUnder:  string;
         AffectGrossProfit: number; 
         ExtraDate: string ; 
         Narration:string; 
         Nature: string; 
         Extra1: string; 
         Extra2: string; 
}