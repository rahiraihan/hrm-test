import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AccountGroupDetailService } from './services/account-group-detail.service';

@Component({
  selector: 'app-account-group-detail',
  templateUrl: './account-group-detail.component.html',
  styleUrls: ['./account-group-detail.component.css']
})
export class AccountGroupDetailComponent implements OnInit {
  public _accountForm: FormGroup;
  constructor(private _formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _accountservice: AccountGroupDetailService,
    private dialogRef: MatDialogRef<AccountGroupDetailComponent>) { }

    ngOnInit() {
      this._accountForm = this._formBuilder.group({
        AccountGroupDetailId: [this.data.AccountGroupDetailId],
        AccountGroupName: [ this.data.AccountGroupName, [Validators.required]],
        GroupUnder: [ this.data.GroupUnder, [Validators.required]],
        AffectGrossProfit: [ this.data.AffectGrossProfit, [Validators.required]],
        ExtraDate: [this.data.ExtraDate, [Validators.required]],
        Narration: [ this.data.Narration, [Validators.required]],
        Nature: [ this.data.Nature, [Validators.required]],
        Extra1: [ this.data.Extra1, [Validators.required]],
        Extra2: [ this.data.Extra2, [Validators.required]]
     });
    }
    onSubmit() {
      if (isNaN(this.data.ID)) {
        this._accountservice.addAccountDetail(this._accountForm.value);
        this.dialogRef.close();
      } else {
        this._accountservice.deleteAccountDetail(this._accountForm.value);
        this.dialogRef.close();
      }
    }
    onClose() {
      this.dialogRef.close();
    }

}
