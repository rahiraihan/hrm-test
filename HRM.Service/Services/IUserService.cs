﻿using HRM.Data.Entities;
using HRM.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace HRM.Service.Services
{
    public interface IUserService
    {
        Task<List<Users>> GetUser(int id);
        Task<Users> CreateNewUser(Users users);
        Task<String> DeleteUser(int id);
        Task<String> UpdateUser(Users users);
        Task<Users> LoginUserTokenBase(String UserName, String Password);
    }
    public class UserService : IUserService
    {
        private readonly IUserRepository _repository;

        public UserService(IUserRepository repository)
        {
            _repository = repository;
        }
        public async Task<Users> CreateNewUser(Users users)
        {
            try
            {
                var createNewUser = await _repository.CreateNewUser(users);
                return createNewUser;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public async Task<string> DeleteUser(int id)
        {
            try
            {
                var deleteUser = await _repository.DeleteUser(id);
                return deleteUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Users>> GetUser(int id)
        {
            try
            {
                var getUser = await _repository.GetUser(id);
                return getUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> UpdateUser(Users users)
        {
            try
            {
                var updateUser = await _repository.UpdateUser(users);
                return updateUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Users> LoginUserTokenBase(String UserName, String Password) {

            try
            {
                var loginUser = await _repository.LoginUserTokenBase(UserName, Password);
                return loginUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
