﻿using HRM.Data.Entities;
using HRM.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HRM.Service.Services
{
   public interface ITaxService
    {
        Task<List<Taxes>> GetTax(int id);
    }

    public class TaxService : ITaxService
    {
        private readonly ITaxRepository _repository;
        public TaxService(ITaxRepository repository)
        {
            _repository = repository;
        }
        public async Task<List<Taxes>> GetTax(int id)
        {
            try
            {
                var getTax = await _repository.GetTax(id);
                return getTax;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
