﻿using HRM.Data.Entities;
using HRM.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HRM.Service.Services
{
    public interface IBrandService
    {
        Task<List<Brands>> GetAllBrands(int id);
    }

    public class BrandService : IBrandService
    {
        private readonly IBrandRepository _repository;

        public BrandService(IBrandRepository repository)
        {
            _repository = repository;
        }
        public async Task<List<Brands>> GetAllBrands(int id)
        {
            try
            {
                var getBrand = await _repository.GetAllBrands(id);
                return getBrand;
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
}
