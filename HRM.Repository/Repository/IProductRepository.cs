﻿using HRM.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace HRM.Repository.Repository
{
    public interface IProductRepository
    {
        Task<List<Product>> GetProduct();
        Task<Product> CreateProduct(Product product);
        Task<String> DeleteProduct(int id);
        Task<String> UpdateProduct(Product product);
    }
    public class ProductRepository : IProductRepository
    {
        private readonly DataContext _context;

        public ProductRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<Product> CreateProduct(Product product)
        {
            try
            {
                Product prod = new Product();
                _context.Product.Add(product);
                await _context.SaveChangesAsync();
                return prod;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> DeleteProduct(int id)
        {
            try
            {
                var deleteProduct = await _context.Product.FindAsync(id);
                _context.Product.Remove(deleteProduct);
                await _context.SaveChangesAsync();
                return "Deleted";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Product>> GetProduct()
        {
            try
            {
                var getProduct = from c in _context.Product
                                 orderby c.ProductAddId descending
                                 select c;
                return await getProduct.ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> UpdateProduct(Product product)
        {
            try
            {
                var updateProduct = _context.Product.FirstOrDefault(x => x.ProductAddId == product.ProductAddId);
                _context.Product.Update(updateProduct);
                await _context.SaveChangesAsync();
                return "Updated";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
