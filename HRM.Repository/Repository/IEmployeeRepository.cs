﻿using HRM.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace HRM.Repository.Repository
{
    public interface IEmployeeRepository
    {
        Task<List<Employees>> GetAllEmployee();
        Task<Employees> CreateAllEmployee(Employees emp);
        Task<String> DeleteEmployee(int id);
        Task<String> UpdateEmployee(Employees emp);
    }

    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly DataContext _context;

        public EmployeeRepository(DataContext context)
        {
            _context = context;
        }


        public async Task<Employees> CreateAllEmployee(Employees emp)
        {
            try
            {
                Employees employees = new Employees();
                _context.Employees.Add(emp);
                await _context.SaveChangesAsync();
                return employees;
            }
            catch (Exception ex) {
                throw ex;
            }

        }

        public async Task<string> DeleteEmployee(int id)
        {
            try
            {
                var deleteEmployee = await _context.Employees.FindAsync(id);
                _context.Employees.Remove(deleteEmployee);
                await _context.SaveChangesAsync();
                return "Deleted";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<List<Employees>> GetAllEmployee()
        {
            try
            {
                var getEmployee = from c in _context.Employees
                                  orderby c.EmployeeId descending
                                  select c;

                return getEmployee.ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> UpdateEmployee(Employees emp)
        {
            try
            {
                var updateEmployee =  _context.Employees.FirstOrDefault( x => x.EmployeeId == emp.EmployeeId);
                _context.Employees.Update(updateEmployee);
                await _context.SaveChangesAsync();
                return "Updated";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
