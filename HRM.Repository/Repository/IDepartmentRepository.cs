﻿using HRM.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace HRM.Repository.Repository
{
    public interface IDepartmentRepository
    {
        Task<List<Departments>> GetDepartment();
        Task<Departments> CreateDepartment(Departments departments);
        Task<String> DeleteDepartment(int id);
        Task<String> UpdateDepartment(Departments departments);
    }
    public class DepartmentRepository : IDepartmentRepository
    {
        private readonly DataContext _context;

        public DepartmentRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<Departments> CreateDepartment(Departments departments)
        {
            try
            {
                Departments department = new Departments();
                _context.Departments.Add(departments);
                await _context.SaveChangesAsync();
                return department;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> DeleteDepartment(int id)
        {
            try
            {
                var deleteDepartment = await _context.Departments.FindAsync(id);
                _context.Departments.Remove(deleteDepartment);
                await _context.SaveChangesAsync();
                return "Deleted";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<List<Departments>> GetDepartment()
        {
            try
            {
                var getDepartment = from c in _context.Departments
                                    orderby c.DepartmentId descending
                                    select c;
                return getDepartment.ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> UpdateDepartment(Departments departments)
        {
            try
            {
                var updateDepartment =  _context.Departments.FirstOrDefault(x => x.DepartmentId == departments.DepartmentId);
                _context.Departments.Update(updateDepartment);
                await _context.SaveChangesAsync();
                return "Updated";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
