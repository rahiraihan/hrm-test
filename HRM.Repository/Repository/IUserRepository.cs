﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using HRM.Data.Entities;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.Extensions.Options;
using HRM.Data.Helpers;

namespace HRM.Repository.Repository
{
    public interface IUserRepository
    {
        Task<List<Users>> GetUser(int id);
        Task<Users> CreateNewUser(Users users);
        Task<String> DeleteUser(int id);
        Task<String> UpdateUser(Users users);
        Task<Users> LoginUserTokenBase(String UserName, String Password);
    }
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;
        private readonly AppSettings _appSettings;

        public UserRepository(DataContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
        }
        public async Task<Users> CreateNewUser(Users users)
        {
            try
            {
                Users user = new Users();
                _context.Users.Add(users);
                await _context.SaveChangesAsync();
                return user;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public async Task<Users> LoginUserTokenBase(String UserName, String Password)
        {
            try
            {
                Users users = new Users();
                var user =  _context.Users.SingleOrDefault(x => x.UserName == UserName && x.Password == Password);
                if (user == null)
                    return null;

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Name, user.UserId.ToString()),
                    new Claim(ClaimTypes.Actor, user.UserName),
                    new Claim(ClaimTypes.Role, user.Role),
                    }),



                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                //var identity = new ClaimsIdentity();
                //identity.AddClaim(new Claim(ClaimTypes.Name, user.UserId.ToString()),
                //identity.AddClaim(new Claim(ClaimTypes.Actor, user.UserName),
                //identity.AddClaim(new Claim(ClaimTypes.Role, user.Role),

                var token = tokenHandler.CreateToken(tokenDescriptor);
                 user.Token =  tokenHandler.WriteToken(token);

                return users;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> DeleteUser(int id)
        {
            try
            {
                var deleteUser = await _context.Users.FindAsync(id);
                _context.Users.Remove(deleteUser);
                await _context.SaveChangesAsync();
                return "Deleted";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private IEnumerable<Claim> GetUserClaims(Users user)
        {
            List<Claim> claims = new List<Claim>();
            Claim _claim;
            _claim = new Claim(ClaimTypes.Name, user.UserId.ToString());
            claims.Add(_claim);
            _claim = new Claim(ClaimTypes.Actor, user.UserName);
            claims.Add(_claim);
            _claim = new Claim(ClaimTypes.Role, user.Role);
            claims.Add(_claim);

            if (user != null)
            {
                claims.Add(_claim);
            }
            return claims.AsEnumerable<Claim>();
        }


        public async Task<List<Users>> GetUser(int id)
        {
            try
            {
        
                var getUser = from c in _context.Users
                              orderby c.UserId descending
                              select c;
                return await getUser.ToListAsync();
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public async Task<string> UpdateUser(Users users)
        {
            try
            {
                var updateUser = _context.Users.FirstOrDefault(x => x.UserId == users.UserId);
                _context.Users.Update(updateUser);
                await _context.SaveChangesAsync();
                return "Updated";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
