﻿using HRM.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace HRM.Repository.Repository
{
   public interface ITaxRepository
   {
        Task<List<Taxes>> GetTax(int id);
        Task<Taxes> CreateTax(Taxes taxes);
        Task<String> DeleteTax(int id);
        Task<String> UpdateTax(Taxes taxes);
    }
    public class TaxRepository : ITaxRepository
    {
        private readonly DataContext _context;

        public TaxRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<Taxes> CreateTax(Taxes taxes)
        {
            try
            {
                Taxes tax = new Taxes();
                _context.Taxes.Add(taxes);
                await _context.SaveChangesAsync();
                return tax;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> DeleteTax(int id)
        {
            try
            {
                var deleteTax = _context.Taxes.Find(id);
                _context.Taxes.Remove(deleteTax);
                await _context.SaveChangesAsync();
                return "Deleted";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Taxes>> GetTax(int id)
        {
            try
            {
                var getTax = from c in _context.Taxes
                             orderby c.TaxesId descending
                             select c;
                return await getTax.ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> UpdateTax(Taxes taxes)
        {
            try
            {
                var updateTax =  _context.Taxes.FirstOrDefault(x => x.TaxesId ==taxes.TaxesId);
                _context.Taxes.Update(updateTax);
                await _context.SaveChangesAsync();
                return "Updated";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
