﻿using HRM.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace HRM.Repository.Repository
{
    public interface IBrandRepository
    {
        Task<List<Brands>> GetAllBrands(int id);
        Task<Brands> CreateAllBrands(Brands brand);
        Task<String> DeleteAllBrands(int id);
        Task<String> UpdateAllBrands(Brands brand);
    }
    public class BrandRepository : IBrandRepository
    {
        private readonly DataContext _context;

        public BrandRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<Brands> CreateAllBrands(Brands brand)
        {
            try
            {
                Brands brands = new Brands();
                _context.Brands.Add(brand);
                await _context.SaveChangesAsync();
                return brands;
            }
            catch (Exception ex) {
                throw ex;
            }

        }

        public async Task<string> DeleteAllBrands(int id)
        {
            try
            {
               
                var deleteBrand = await _context.Brands.FindAsync(id);
                await _context.SaveChangesAsync();
                return "Deleted";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<List<Brands>> GetAllBrands(int id)
        {
            try
            {
                var getBrand = from c in _context.Brands
                               orderby c.BrandsAddId descending
                               select c;
                return getBrand.ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> UpdateAllBrands(Brands brand)
        {
            try
            {
                
                var entity = _context.Brands.FirstOrDefault(x => x.BrandsAddId == brand.BrandsAddId);
                _context.Brands.Update(entity);
                await _context.SaveChangesAsync();
                return "Updated User Successfully";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
