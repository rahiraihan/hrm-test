﻿using System;
using System.Collections.Generic;

namespace HRM.Data.Entities
{
    public partial class Employees
    {
        public int EmployeeId { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeLastName { get; set; }
        public string EmployeeFullName { get; set; }
        public string EmployeePhoneNumber { get; set; }
        public DateTime EmployeeHireDate { get; set; }
        public decimal Salary { get; set; }
        public decimal? CommissionPercent { get; set; }
        public int? UserId { get; set; }

        public virtual Users User { get; set; }
    }
}
