﻿using System;
using System.Collections.Generic;

namespace HRM.Data.Entities
{
    public partial class Product
    {
        public int ProductAddId { get; set; }
        public string ProductName { get; set; }
        public string ProductGroup { get; set; }
        public int? Barcode { get; set; }
        public string Brand { get; set; }
        public int? MinimumStock { get; set; }
        public string Size { get; set; }
        public string Narration { get; set; }
        public int? Tax { get; set; }
        public string OpeningStock { get; set; }
        public int? Vat { get; set; }
        public string Status { get; set; }
        public int? UserId { get; set; }

        public virtual Users User { get; set; }
    }
}
