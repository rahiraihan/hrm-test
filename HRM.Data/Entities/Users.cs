﻿using System;
using System.Collections.Generic;

namespace HRM.Data.Entities
{
    public partial class Users
    {
        public Users()
        {
            Brands = new HashSet<Brands>();
            Departments = new HashSet<Departments>();
            Employees = new HashSet<Employees>();
            Product = new HashSet<Product>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserGender { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public DateTime? LoginDate { get; set; }

        public virtual ICollection<Brands> Brands { get; set; }
        public virtual ICollection<Departments> Departments { get; set; }
        public virtual ICollection<Employees> Employees { get; set; }
        public virtual ICollection<Product> Product { get; set; }
    }
}
