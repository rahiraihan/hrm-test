﻿using System;
using System.Collections.Generic;

namespace HRM.Data.Entities
{
    public partial class Departments
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int UserId { get; set; }
        public int Attendance { get; set; }
        public DateTime AttendanceDate { get; set; }

        public virtual Users User { get; set; }
    }
}
