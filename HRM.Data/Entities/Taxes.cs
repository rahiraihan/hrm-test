﻿using System;
using System.Collections.Generic;

namespace HRM.Data.Entities
{
    public partial class Taxes
    {
        public int TaxesId { get; set; }
        public string TaxName { get; set; }
        public int? TaxValue { get; set; }
        public string Status { get; set; }
    }
}
